#--------------------------------------------------------------------------
# feature: features from benchmark code
#--------------------------------------------------------------------------
include Makefile

FEATURE_NAME := f

FEATURE_TRN1 := $(DIR_FEATURE)/trn1.vw
FEATURE_VAL1 := $(DIR_FEATURE)/val1.vw
FEATURE_TRN := $(DIR_FEATURE)/train.vw
FEATURE_TST := $(DIR_FEATURE)/test.vw

SVM_SCALE := ~/libsvm-3.18/svm-scale

Y_VAL1 := $(DIR_FEATURE)/y.val1.txt

$(Y_VAL): $(FEATURE_TRN)
	cut -d" " -f1 $< > $@
python tsv2vw.py train.tsv train.vw
$(FEATURE_TRN): $(DIR_DATA)/$(DATA_TRN) | $(DIR_FEATURE) $(DIR_DATA)
	python src/kaggle-avito/tsv2vw.py $< $@

$(FEATURE_TST): $(DIR_DATA)/$(DATA_TST) | $(DIR_FEATURE) $(DIR_DATA)
	python src/kaggle-avito/tsv2vw.py $< $@

$(FEATURE_VAL1): $(FEATURE_TRN) | $(DIR_FEATURE)
	head -n 1351242 $< > $@

$(FEATURE_TRN1): $(FEATURE_TRN) | $(DIR_FEATURE)
	tail -n +1351243 $< > $@

$(FEATURE_VAL1).scale $(FEATURE_TRN1).scale: $(FEATURE_VAL1) $(FEATURE_TRN1) | $(DIR_FEATURE)
	$(SVM_SCALE) -l 0 -u 1 -s $(FEATURE_TRN1)_scale_file $(FEATURE_TRN1) > $(FEATURE_TRN1).scale
	$(SVM_SCALE) -r $(FEATURE_TRN1)_scale_file $(FEATURE_VAL1) > $(FEATURE_VAL1).scale

$(FEATURE_TST).scale $(FEATURE_TRN).scale: $(FEATURE_TST) $(FEATURE_TRN) | $(DIR_FEATURE)
	$(SVM_SCALE) -l 0 -u 1 -s $(FEATURE_TRN)_scale_file $(FEATURE_TRN) > $(FEATURE_TRN).scale
	$(SVM_SCALE) -r $(FEATURE_TRN)_scale_file $(FEATURE_TST) > $(FEATURE_TST).scale