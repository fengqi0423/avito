#!/usr/bin/env python
import argparse
import numpy as np
from sklearn.datasets import dump_svmlight_file
import errno, sys

from kaggler.util import normalize_numerical_feature2

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--feature-file', '-f', required=True, dest='feature_file')
    parser.add_argument('--predict-files', '-p', required=True,
    dest='predict_files', nargs='+')
    parser.add_argument('--target-file', '-t', required=False, dest='target_file', default=None)

    args = parser.parse_args()

    
    features = []
    for f in args.predict_files:
        yhat = normalize_numerical_feature2(np.loadtxt(f))
        features.append(yhat)

    if len(features) == 0:
        sys.exit(errno.EINVAL) 
    features = np.matrix(features).transpose()

    print args.predict_files
    if args.target_file:
        y = np.loadtxt(args.target_file)
    else:
        y = [0, ] * features.shape[0]

    dump_svmlight_file(features, y, args.feature_file, zero_based=False)

