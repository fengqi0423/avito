# coding: utf-8

import argparse
import numpy as np
import os
import pandas as pd
import re
from scipy import sparse
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.datasets import dump_svmlight_file
import random
from logger import log as logger
import nltk.corpus
from nltk import SnowballStemmer
import re
import csv
from string import maketrans
from utility import encode_categorical_feature

STOPWORDS= frozenset(word.decode('utf-8') for word in nltk.corpus.stopwords.words("russian") if word!="не")    
stemmer = SnowballStemmer('russian')

NUM_COLUMNS = ['price','phones_cnt','emails_cnt','urls_cnt']

ID_COLUMNS = ['itemid']

TEXT_COLUMNS = ['title','description', 'attrs']

CAT_COLUMNS = ['category', 'subcategory']

def text_analyzer(text, min_length=2, ngrams=1):
    if not text:
        return []
    
    cleanText = re.sub(u'[^a-zа-я0-9]', ' ', text.lower())
    words = [ w for w in cleanText.split() if len(w)>min_length and w not in STOPWORDS]
    tks = [ w if re.search("[0-9a-z]", w) else stemmer.stem(w) for w in words]

    if ngrams < 2:
        return tks
    for i in range(2, int(ngrams) + 1):
        tks = tks + nltk.ngrams(tks,i)

    return tks

def decode_utf_8(s):
    return s.decode('utf-8')

def log_feature_cnt(X_trn, X_tst):
    logger.info('TRN {} features'.format(X_trn.shape[1]))
    logger.info('TST {} features'.format(X_tst.shape[1]))
    if X_trn.shape[1] != X_tst.shape[1]:
        logger.info('WARNING, feature cnt does not match!!!!')
 
def generate_feature(data_dir, train_input_file, test_input_file,
                     train_feature_file, valid_feature_file,
                     train1_feature_file, test_feature_file):
    # load data files
    logger.info('loading data files')

    trn = pd.read_csv(os.path.join(data_dir, train_input_file), delimiter='\t')
    tst = pd.read_csv(os.path.join(data_dir, test_input_file), delimiter='\t')

    trn.fillna("", inplace=True)
    tst.fillna("", inplace=True)
    
    for column_name in TEXT_COLUMNS + CAT_COLUMNS:
        trn[column_name] = trn[column_name].apply(decode_utf_8)
        tst[column_name] = tst[column_name].apply(decode_utf_8)
    
    # set target labels
    y_trn = np.array(trn.is_blocked)
    y_tst = np.zeros(len(tst))
    
    i_trn1 = np.where(np.array(range(len(y_trn))) >= len(y_tst))[0]
    i_val1 = np.where(np.array(range(len(y_trn))) < len(y_tst))[0]
    
    logger.info('loading data files text part')
    # text col
    
    trn_arr = np.array(trn[TEXT_COLUMNS])
    tst_arr = np.array(tst[TEXT_COLUMNS])

    # numerical variables
    X_trn = np.array(trn[NUM_COLUMNS])
    X_tst = np.array(tst[NUM_COLUMNS])    
    log_feature_cnt(X_trn, X_tst)

    # category variables
    for col in CAT_COLUMNS:
        logger.info('transforming feature {}'.format(col))
        
        trn_col, tst_col = encode_categorical_feature(trn[col],
                                                      tst[col],
                                                      min_obs=50)
        X_trn = sparse.hstack((X_trn, trn_col))
        X_tst = sparse.hstack((X_tst, tst_col))
        log_feature_cnt(X_trn, X_tst)

    # build TF-IDF features from essays
    vec_title = TfidfVectorizer(max_df = 0.5, min_df=0.00001,  max_features=None, strip_accents='utf-8',  
                                analyzer=text_analyzer, use_idf=True,smooth_idf=True,sublinear_tf=True, binary=False)

    vec_desp = TfidfVectorizer(max_df = 0.5, min_df=0.00001,  max_features=None, strip_accents='utf-8',  
                                analyzer=text_analyzer, use_idf=True,smooth_idf=True,sublinear_tf=True, binary=False)

    vec_attr = TfidfVectorizer(max_df = 0.5, min_df=0.00001,  max_features=None, strip_accents='utf-8',  
                                analyzer=text_analyzer, use_idf=True,smooth_idf=True,sublinear_tf=True, binary=False)

    logger.info('building the TF-IDF dictionary for titles')
    vec_title = vec_title.fit(trn_arr[:, 0])

    logger.info('building the TF-IDF dictionary for descriptions')
    vec_desp = vec_desp.fit(trn_arr[:, 1])

    logger.info('building the TF-IDF dictionary for attr')
    vec_attr = vec_attr.fit(trn_arr[:, 2])

    logger.info('transforming the TF-IDF dictionary for titles')
    X_trn = sparse.hstack((X_trn, vec_title.transform(trn_arr[:, 0])))

    logger.info('transforming the TF-IDF dictionary for titles tst data')
    X_tst = sparse.hstack((X_tst, vec_title.transform(tst_arr[:, 0])))
    log_feature_cnt(X_trn, X_tst)

    logger.info('transforming the TF-IDF dictionary for descriptions')
    X_trn = sparse.hstack((X_trn, vec_desp.transform(trn_arr[:, 1])))

    logger.info('transforming the TF-IDF dictionary for descriptions tst data')
    X_tst = sparse.hstack((X_tst, vec_desp.transform(tst_arr[:, 1])))
    log_feature_cnt(X_trn, X_tst)

    logger.info('transforming the TF-IDF dictionary for attr')
    X_trn = sparse.hstack((X_trn, vec_attr.transform(trn_arr[:, 2])))

    logger.info('transforming the TF-IDF dictionary for attr tst data')
    X_tst = sparse.hstack((X_tst, vec_attr.transform(tst_arr[:, 2])))
    log_feature_cnt(X_trn, X_tst)

    X_trn = X_trn.tocsr()
    X_tst = X_tst.tocsr()
    # save features as sparse matrix files
    logger.info('saving training1 features')
    dump_svmlight_file(X_trn[i_trn1], y_trn[i_trn1], train1_feature_file, zero_based=False)

    logger.info('saving val1 features')
    dump_svmlight_file(X_trn[i_val1], y_trn[i_val1], valid_feature_file, zero_based=False)

    logger.info('saving training features')
    dump_svmlight_file(X_trn, y_trn, train_feature_file, zero_based=False)

    logger.info('saving test features')
    dump_svmlight_file(X_tst, y_tst, test_feature_file, zero_based=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data-dir', required=True, dest='data_dir')
    parser.add_argument('--train-input-file', required=True,
                        dest='train_input')
    parser.add_argument('--test-input-file', required=True,
                        dest='test_input')
    parser.add_argument('--train-feature-file', required=True,
                        dest='train_feature')
    parser.add_argument('--valid-feature-file', required=True,
                        dest='valid_feature')
    parser.add_argument('--train1-feature-file', required=True,
                        dest='train1_feature')
    parser.add_argument('--test-feature-file', required=True,
                        dest='test_feature')

    args = parser.parse_args()

    generate_feature(data_dir=args.data_dir,
                     train_input_file=args.train_input,
                     test_input_file=args.test_input,
                     train_feature_file=args.train_feature,
                     valid_feature_file=args.valid_feature,
                     train1_feature_file=args.train1_feature,
                     test_feature_file=args.test_feature)
