# coding: utf-8

from __future__ import division
import argparse
import pandas as pd
import numpy as np
from sklearn.datasets import load_svmlight_file, dump_svmlight_file
import time

from kaggler.logger import log


def generate_feature(feature2_file, feature4_file, weight_file):

    log.info('loading the weight file')
    w = np.loadtxt(weight_file, skiprows=6)

    df = pd.DataFrame({'abs_weight': np.abs(w[: -1])},
                      index=range(w.shape[0] - 1))

    df.sort('abs_weight', ascending=False, inplace=True)

    idx = np.array(df.index[:2000])

    log.info('loading the feature2 file')
    X, y = load_svmlight_file(feature2_file)

    log.info('selecting top 2000 features from feature2')
    X = X[:, idx]
    
    log.info('saving the feature4 file')
    dump_svmlight_file(X, y, feature4_file, zero_based=False)

                               
if __name__=="__main__":            
    parser = argparse.ArgumentParser()
    parser.add_argument('--feature2-file', required=True, dest='feature2')
    parser.add_argument('--feature4-file', required=True, dest='feature4')
    parser.add_argument('--weight-file', required=True, dest='weight')

    args = parser.parse_args()

    start = time.time()
    generate_feature(feature2_file=args.feature2,
                     feature4_file=args.feature4,
                     weight_file=args.weight)

    log.info('finished ({:.2f} min elapsed)'.format((time.time() - start) / 60.))
