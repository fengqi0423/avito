#!/usr/bin/env python
import argparse
import numpy as np
import os
from sklearn import metrics

def evaluate_auc(y, yhat):
    """
    Calculate AUC. https://www.kaggle.com/wiki/AreaUnderCurve

    Args:
        --y: Real label of each sample.
        --yhat: Predicted result of each sample.
    Outputs:
        AUC. 
    """

    return metrics.roc_auc_score(y, yhat)

def evaluate_APatK(y, yhat, k):
    """
    Calculate AP@K. https://www.kaggle.com/wiki/MeanAveragePrecision

    Args:
        --y: Real label of each sample.
        --yhat: Predicted result of each sample.
        --k: Only consider top k results.
    Outputs:
        AP@K. 
    """

    sorted_yhat = sorted(zip(range(len(yhat)), yhat), key=lambda x: x[1], reverse=True)
    
    return apk(np.where(np.array(y) >0)[0], np.array([ x[0] for x in sorted_yhat]),k)

def apk(actual, predicted, k=10):
    """
    Calculate AP@K. https://www.kaggle.com/wiki/MeanAveragePrecision

    Args:
        --actual: All actual positive samples.
        --predicted: Samples order by predict probability.
        --k: Only consider top k results.
    Outputs:
        AP@K. 
    """

    if len(predicted)>k:
        predicted = predicted[:k]

    score = 0.0
    num_hits = 0.0

    if actual is None:
        return 0.0

    if k <= 0:
        k = 1

    for i,p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i+1.0)

    if actual is None:
        return 0.0

    return score / min(len(actual), k)

def load_data(target_file, predict_file):
    """
    Read label data file and predict result file.

    Args:
        --target_file: A path to label file.
        --predict_file: A path to predict result file.
        All these files should only include one column and each row must be the same sample.
    Outputs:
         a tuple of array of label and array of predict result

    """

    return np.loadtxt(target_file), np.loadtxt(predict_file)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--target-file', '-t', required=True, dest='target_file')
    parser.add_argument('--predict-file', '-p', required=True, dest='predict_file')
    parser.add_argument('--k', '-k', required=False, dest='k', default=65000)

    args = parser.parse_args()

    y, yhat = load_data(target_file=args.target_file, predict_file=args.predict_file)
    auc = evaluate_auc(y=y, yhat=yhat)

    apk_32500 = evaluate_APatK(y=y, yhat=yhat,k=32500)
    apk_65000 = evaluate_APatK(y=y, yhat=yhat,k=65000)
    model_name = os.path.splitext(args.predict_file)[0]
    print('{}\t{:.6f}\t{:.6f}\t{:.6f}'.format(model_name, auc, apk_65000, apk_32500))
