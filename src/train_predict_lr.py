#!/usr/bin/env python

import argparse
import numpy as np
from sklearn import cross_validation, metrics
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
import time

from const import FIXED_SEED
from logger import log
from utility import load_data


def train_predict_lr_cv(train_file, test_file, predict_valid_file,
                        predict_test_file, c):
    log.info("reading in the training data")
    X_trn, y_trn = load_data(train_file)

    log.info("reading in the test data")
    X_tst, _ = load_data(test_file)

    log.info('normalizing data')
    scaler = StandardScaler(with_mean=False)
    X_trn = scaler.fit_transform(X_trn)
    X_tst = scaler.transform(X_tst)
 
    n_tst = X_tst.shape[0]
    n_val = n_tst

    log.info('training for validation')
    clf = LogisticRegression(C=c, penalty='l2', dual=False,
                             class_weight='auto',
                             random_state=FIXED_SEED)
    clf.fit(X_trn[n_val:], y_trn[n_val:])

    yhat_val = clf.predict_proba(X_trn[:n_val])[:, 1]

    log.info("writing validation predictions to file")
    np.savetxt(predict_valid_file, yhat_val, fmt='%.6f', delimiter=',')

    log.info('training for submission')
    clf = LogisticRegression(C=c, penalty='l2', dual=False,
                             class_weight='auto',
                             random_state=FIXED_SEED)
    clf.fit(X_trn, y_trn)

    yhat_tst = clf.predict_proba(X_tst)[:, 1]

    log.info("writing test predictions to file")
    np.savetxt(predict_test_file, yhat_tst, fmt='%.6f', delimiter=',')

    
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', '-t', required=True, dest='train')
    parser.add_argument('--test-file', '-v', required=True, dest='test')
    parser.add_argument('--predict-valid-file', '-p', required=True,
                        dest='predict_valid')
    parser.add_argument('--predict-test-file', '-q', required=True,
                        dest='predict_test')
    parser.add_argument('--c', '-c', required=True, type=float, dest='c')

    args = parser.parse_args()

    start = time.time()
    train_predict_lr_cv(train_file=args.train,
                        test_file=args.test,
                        predict_valid_file=args.predict_valid,
                        predict_test_file=args.predict_test,
                        c=args.c)

    log.info('finished ({:.2f} sec elasped).'.format(time.time() - start))
