# coding: utf-8

from __future__ import division
import argparse
from nltk import SnowballStemmer, word_tokenize
from nltk.corpus import stopwords
import numpy as np
import pandas as pd
from scipy import sparse
from sklearn.datasets import dump_svmlight_file
from sklearn.feature_extraction.text import TfidfVectorizer
import time

from logger import log as logger
from utility import encode_categorical_feature, normalize_numerical_feature2


STOPWORDS= frozenset(word.decode('utf-8') for word in stopwords.words("russian") if word!="??")
stemmer = SnowballStemmer('russian')
CAT_COLS = ['category', 'subcategory']
NUM_COLS = ['emails_cnt', 'phones_cnt', 'price', 'urls_cnt']


def stem_tokens(tokens, stemmer):                                               
    stemmed = []                                                                
    for item in tokens:
        stemmed.append(stemmer.stem(item))                                      
    return stemmed 


def tokenize(s):
    tokens = word_tokenize(s)                                                   
    stems = stem_tokens(tokens, stemmer)                                        
    return stems  


def generate_feature(train_file, test_file, train_feature_file,
                     test_feature_file):

    logger.info('loading training data')
    df = pd.read_csv(train_file, sep='\t', encoding='utf-8')
    n_trn = df.shape[0]
    y_trn = np.array(df.is_blocked, dtype=int)

    # drop columns for additional information for training data
    df.drop(['is_proved', 'close_hours'], axis=1, inplace=True)

    logger.info('loading test data')
    tst = pd.read_csv(test_file, sep='\t', encoding='utf-8')
    tst['is_blocked'] = 0

    # combine training and test data
    df = pd.concat([df, tst], axis=0)

    df.title.fillna('__no_text__', inplace=True)
    df.description.fillna('__no_text__', inplace=True)
    df.attrs.fillna('__no_text__', inplace=True)

    # start with numerical features
    X = np.array(df[NUM_COLS])
    logger.info('{} features'.format(X.shape[1])) 

    # one-hot-encoding for categorical variables
    for col in CAT_COLS:
        logger.info('transforming feature {}'.format(col))
        X = sparse.hstack((X, encode_categorical_feature(df[col],
                                                         min_obs=4000)[0]))
        logger.info('{} features'.format(X.shape[1]))

    logger.info('converting the sparse feature matrix to the CSR format')

    # add bag-of-words features
    logger.info('building title bag-of-words features')
    vec_title = TfidfVectorizer(stop_words=STOPWORDS, tokenizer=tokenize,
                                ngram_range=(1, 1), max_df=0.9, min_df=0.001)
    X = sparse.hstack((X, vec_title.fit_transform(df.title)))
    logger.debug('{} features'.format(X.shape[1])) 

    logger.info('building description bag-of-words features')
    vec_desc = TfidfVectorizer(stop_words=STOPWORDS, tokenizer=tokenize,
                               ngram_range=(1, 1), max_df=0.9, min_df=0.001)
    X = sparse.hstack((X, vec_desc.fit_transform(df.description)))
    logger.debug('{} features'.format(X.shape[1])) 

    logger.info('building attribution bag-of-words features')
    vec_attr = TfidfVectorizer(stop_words=STOPWORDS, tokenizer=tokenize,
                               ngram_range=(1, 1), max_df=0.9, min_df=0.001)
    X = sparse.hstack((X, vec_attr.fit_transform(df.attrs)))
    logger.debug('{} features'.format(X.shape[1])) 

    logger.info('converting the sparse feature matrix to the CSR format')
    X = X.tocsr()

    logger.info('saving training features')
    dump_svmlight_file(X[:n_trn], y_trn, train_feature_file, zero_based=False)

    logger.info('saving test features')
    dump_svmlight_file(X[n_trn:], np.zeros((tst.shape[0], )),
                       test_feature_file, zero_based=False)
    

                               
if __name__=="__main__":            
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train')
    parser.add_argument('--test-file', required=True, dest='test')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature')

    args = parser.parse_args()

    start = time.time()
    generate_feature(train_file=args.train,
                     test_file=args.test,
                     train_feature_file=args.train_feature,
                     test_feature_file=args.test_feature)

    logger.info('finished ({:.2f} min elapsed)'.format((time.time() - start) / 60.))
