﻿#!/usr/bin/env python

from __future__ import division
import argparse
from nltk import SnowballStemmer, word_tokenize
from nltk.corpus import stopwords
import numpy as np
import pandas as pd
import re
from scipy import sparse
from sklearn.datasets import dump_svmlight_file
from sklearn.feature_extraction.text import TfidfVectorizer
import time

from kaggler.logger import log
from kaggler.util import encode_categorical_feature, normalize_numerical_feature2


STOPWORDS= frozenset(word.decode('utf-8') for word in stopwords.words("russian") if word!="не")
stemmer = SnowballStemmer('russian')
engChars = [ord(char) for char in u"cCyoOBaAKpPeE"]
rusChars = [ord(char) for char in u"сСуоОВаАКрРеЕ"]
eng_rusTranslateTable = dict(zip(engChars, rusChars))
rus_engTranslateTable = dict(zip(rusChars, engChars))

CAT_COLS = ['category', 'subcategory']
NUM_COLS = ['emails_cnt', 'phones_cnt', 'price', 'urls_cnt']
TXT_COLS = ['title', 'description', 'attrs']


def text_analyzer(text, min_length=2, ngrams=1):
    if not text:
        return []
    
    cleanText = re.sub(u'[^a-zа-я0-9]', ' ', text.lower())
    words = [ w for w in cleanText.split() if len(w)>min_length and w not in STOPWORDS]
    tks = [ w if re.search("[0-9a-z]", w) else stemmer.stem(w) for w in words]

    if ngrams < 2:
        return tks
    for i in range(2, int(ngrams) + 1):
        tks = tks + nltk.ngrams(tks,i)

    return tks


def decode_utf_8(s):
    return s.decode('utf-8')


def generate_feature(train_file, test_file, train_feature_file,
                     test_feature_file):

    log.info('loading training data')
    df = pd.read_csv(train_file, sep='\t', encoding='utf-8')
    n_trn = df.shape[0]
    y_trn = np.array(df.is_blocked, dtype=int)

    # drop columns for additional information for training data
    df.drop(['is_proved', 'close_hours'], axis=1, inplace=True)

    log.info('loading test data')
    tst = pd.read_csv(test_file, sep='\t', encoding='utf-8')
    tst['is_blocked'] = 0

    # combine training and test data
    df = pd.concat([df, tst], axis=0)

    # clean text columns
    for col in TXT_COLS:
        df[col].fillna('__no_text__', inplace=True)
    
    # start with numerical features
    for col in NUM_COLS:
        log.info('log-transforming feature {}'.format(col))
        df[col] = df[col].apply(np.log1p)
        
    X = np.array(df[NUM_COLS])
    log.debug('{} features'.format(X.shape[1])) 

    # one-hot-encoding for categorical variables
    for col in CAT_COLS:
        log.info('transforming feature {}'.format(col))
        X = sparse.hstack((X, encode_categorical_feature(df[col],
                                                         min_obs=500)))
        log.debug('{} features'.format(X.shape[1]))

    # add bag-of-words features
    log.info('building title bag-of-words features')
    vec_title = TfidfVectorizer(stop_words=STOPWORDS,
                                analyzer=text_analyzer, strip_accents='utf-8',
                                ngram_range=(2, 2), max_df=0.9, min_df=0.001)
    X = sparse.hstack((X, vec_title.fit_transform(df.title)))
    log.debug('{} features'.format(X.shape[1])) 

    log.info('building description bag-of-words features')
    vec_desc = TfidfVectorizer(stop_words=STOPWORDS,
                               analyzer=text_analyzer, strip_accents='utf-8',
                               ngram_range=(2, 2), max_df=0.9, min_df=0.001)
    X = sparse.hstack((X, vec_desc.fit_transform(df.description)))
    log.debug('{} features'.format(X.shape[1])) 

    log.info('converting the sparse feature matrix to the CSR format')
    X = X.tocsr()

    log.info('saving training features')
    dump_svmlight_file(X[:n_trn], y_trn, train_feature_file, zero_based=False)

    log.info('saving test features')
    dump_svmlight_file(X[n_trn:], np.zeros((tst.shape[0], )),
                       test_feature_file, zero_based=False)
    

                               
if __name__=="__main__":            
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train')
    parser.add_argument('--test-file', required=True, dest='test')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature')

    args = parser.parse_args()

    start = time.time()
    generate_feature(train_file=args.train,
                     test_file=args.test,
                     train_feature_file=args.train_feature,
                     test_feature_file=args.test_feature)

    log.info('finished ({:.2f} min elapsed)'.format((time.time() - start) / 60.))
