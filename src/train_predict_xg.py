#!/usr/bin/env python

import argparse
import numpy as np
import pandas as pd
import time

from const import FIXED_SEED
from logger import log
from utility import load_data
import xgboost as xgb


def train_predict_xg_cv(train_file, test_file, predict_valid_file,
                        predict_test_file, n_iter, depth, lrate):
    param = {'bst:max_depth': depth,
             'bst:eta': lrate,
             'bst:subsample': 0.5,
             'silent': 1,
             'objective': 'binary:logistic',
             'eval_metric': 'auc'}

    log.info("reading in the training data")
    X_trn, y_trn = load_data(train_file)

    log.info("reading in the test data")
    X_tst, _ = load_data(test_file)

    n_tst = X_tst.shape[0]
    n_val = n_tst


    log.info('training for validation')
    dtrain = xgb.DMatrix(X_trn[n_val:].copy())
    dtrain.set_label(y_trn[n_val:].copy())

    dvalid = xgb.DMatrix(X_trn[:n_val].copy())
    dvalid.set_label(y_trn[:n_val].copy())

    evallist = [(dvalid, 'eval'), (dtrain, 'train')]
    bst = xgb.train(param, dtrain, n_iter, evallist)

    yhat_val = bst.predict(dvalid)

    log.info("writing validation predictions to file")
    np.savetxt(predict_valid_file, yhat_val, fmt='%.6f', delimiter=',')

    log.info('training for submission')
    dtrain = xgb.DMatrix(X_trn)
    dtrain.set_label(y_trn)

    evallist = [(dtrain, 'train')]
    bst = xgb.train(param, dtrain, n_iter, evallist)

    dtest = xgb.DMatrix(X_tst)
    yhat_tst = bst.predict(dtest)
    
    log.info("writing test predictions to file")
    np.savetxt(predict_test_file, yhat_tst, fmt='%.6f', delimiter=',')


    
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', '-t', required=True, dest='train')
    parser.add_argument('--test-file', '-v', required=True, dest='test')
    parser.add_argument('--predict-valid-file', '-p', required=True,
                        dest='predict_valid')
    parser.add_argument('--predict-test-file', '-q', required=True,
                        dest='predict_test')
    parser.add_argument('--n-iter', required=True, type=int, dest='n_iter')
    parser.add_argument('--depth', required=True, type=int, dest='depth')
    parser.add_argument('--learn-rate', required=True, type=float, dest='lrate')

    args = parser.parse_args()

    start = time.time()
    train_predict_xg_cv(train_file=args.train,
                        test_file=args.test,
                        predict_valid_file=args.predict_valid,
                        predict_test_file=args.predict_test,
                        n_iter=args.n_iter,
                        depth=args.depth,
                        lrate=args.lrate)

    log.info('finished ({:.2f} sec elasped).'.format(time.time() - start))
