#!/usr/bin/env python
from __future__ import division
import argparse
import cPickle as pkl
import gc
import numpy as np
from sklearn.ensemble import RandomForestClassifier
import time

from const import FIXED_SEED
from logger import log
from utility import load_data


def train_predict_rf(train_file, test_file, predict_file, n_iter, depth):
    log.info("reading the training data")
    X, y = load_data(train_file, dense=True)

    n_obs = X.shape[0]
    n_batch = int(np.ceil(n_obs / 10.))
    idx = range(n_obs)

    np.random.seed(FIXED_SEED)
    np.random.shuffle(idx)
    for i in range(10):
        sub_idx = idx[n_batch * i:n_batch * (i + 1)]
        rf = RandomForestClassifier(n_estimators=n_iter,
                                    max_depth=depth,
                                    random_state=FIXED_SEED,
                                    n_jobs=4)
        log.info('training the RF model #{}'.format(i + 1))
        rf.fit(X[sub_idx], y[sub_idx])

        if i == 0:
            clf = rf
        else:
            clf.estimators_ += rf.estimators_

    with open('rf_model.pkl', 'wb') as f:
        pkl.dump(clf, f)

    del X, y, rf
    gc.collect()

    log.info("reading the test data")
    X, _ = load_data(test_file, dense=True)

    log.info("predicting for the test data")
    yhat = clf.predict_proba(X)[:, 1]

    log.info("writing test predictions to file")
    np.savetxt(predict_file, yhat, fmt='%.6f', delimiter=',')

    
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', '-t', required=True, dest='train')
    parser.add_argument('--test-file', '-v', required=True, dest='test')
    parser.add_argument('--predict-file', '-p', required=True,
                        dest='predict')
    parser.add_argument('--iter', '-n', required=True, type=int, dest='n')
    parser.add_argument('--depth', '-d', required=True, type=int, dest='depth')

    args = parser.parse_args()

    start = time.time()
    train_predict_rf(train_file=args.train,
                     test_file=args.test,
                     predict_file=args.predict,
                     n_iter=args.n,
                     depth=args.depth)

    log.info('finished ({:.2f} sec elasped).'.format(time.time() - start))
