from sklearn.preprocessing import OneHotEncoder
import numpy as np
from scipy.stats import norm
from statsmodels.distributions.empirical_distribution import ECDF
from sklearn.datasets import dump_svmlight_file,load_svmlight_file


def is_number(s):
    """Check if a string is a number or not."""
    try:
        float(s)
        return True
    except ValueError:
        return False


def load_data(path, dense=False):
    """Load data from a CSV or libsvm format file.
    
    Args:
        path: A path to the CSV or libsvm format file containing data.
        dense: An optional variable indicating if the return matrix should be
            dense.  By default, it is false.
    """

    with open(path, 'r') as f:
        line = f.readline().strip()

    if ':' in line:
        X, y = load_svmlight_file(path)
        if dense:
            X = X.astype(np.float32)
            X = X.todense()
    elif ',' in line:
        X = np.loadtxt(path, delimiter=',',
                       skiprows=0 if is_number(line.split(',')[0]) else 1)
        y = X[:, 0]
        X = X[:, 1:]
    else:
        raise NotImplementedError, "Neither CSV nor LibSVM formatted file."

    return X, y


def encode_categorical_feature(trn_feature, tst_feature=[], min_obs=10, n=None):
    """Encode the Pandas column into sparse matrix with one-hot-encoding."""

    if not n:
        n = len(trn_feature)

    label_encoder = get_label_encoder(trn_feature[:n], min_obs)
    trn_labels = trn_feature.apply(lambda x: label_encoder.get(x, 0))
    trn_labels = np.matrix(trn_labels).reshape(len(trn_labels), 1)
    enc = OneHotEncoder()
    trn_encode = enc.fit_transform(trn_labels)

    tst_encode = None
    if len(tst_feature) > 0:
        tst_labels = tst_feature.apply(lambda x: label_encoder.get(x, 0))
        tst_labels = np.matrix(tst_labels).reshape(len(tst_labels), 1)
        tst_encode = enc.transform(tst_labels)

    return trn_encode, tst_encode


def get_label_encoder(feature, min_obs=10):
    label_count = {}
    for label in feature:
        if label in label_count:
            label_count[label] += 1
        else:
            label_count[label] = 1

    label_encoder = {}    
    for label in label_count.keys():
        if label_count[label] >= min_obs:
            label_encoder[label] = len(label_encoder) + 1

    return label_encoder


def normalize_numerical_feature2(feature, n):
    """Normalize the Pandas column based on cumulative distribution.
    
    Args:
        feature: feature vector to normalize.
        n: number of observations to use for deriving probability distribution
           of the feature.  Observations beyond first n observations will be
           normalized based on the probability distribution found from the
           first n observations. 

    Returns:
        A normalized feature vector.
    """

    # add one to the numerator and denominator to avoid +-inf.
    ecdf = ECDF(feature[:n])

    return norm.ppf(ecdf(feature) * .998 + .001)
