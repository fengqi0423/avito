include Makefile.feature.feature3

ITER := 500
DEPTH := 8
LRATE := 0.5
ALGO_NAME := xg2_$(ITER)_$(DEPTH)_$(LRATE)

MODEL_NAME := $(ALGO_NAME)_$(FEATURE_NAME)

METRIC_VAL1 := $(DIR_METRIC)/$(MODEL_NAME).val1.txt

PREDICT_VAL1 := $(DIR_VAL1)/$(MODEL_NAME).val1.yht
PREDICT_TST := $(DIR_TST)/$(MODEL_NAME).tst.yht

SUBMISSION_TST := $(DIR_TST)/$(MODEL_NAME).tst.csv
SUBMISSION_TST_GZ := $(DIR_TST)/$(MODEL_NAME).tst.csv.gz


all: validation submission
validation: $(METRIC_VAL1)
submission: $(SUBMISSION_TST_GZ)
retrain: clean_$(ALGO_NAME) validation

$(PREDICT_VAL1) $(PREDICT_TST): $(FEATURE_TRN) $(FEATURE_TST) | $(DIR_VAL1) $(DIR_TST)
	./src/train_predict_xg.py --train-file=$< \
                              --test-file=$(lastword $^) \
                              --predict-valid-file=$(PREDICT_VAL1) \
                              --predict-test-file=$(PREDICT_TST) \
                              --n-iter=$(ITER) \
                              --depth=$(DEPTH) \
                              --learn-rate=$(LRATE)

$(METRIC_VAL1): $(PREDICT_VAL1) $(Y_VAL1) | $(DIR_METRIC)
	./src/evaluate.py -t $(lastword $^) -p $< > $@
	cat $@

$(SUBMISSION_TST_GZ): $(SUBMISSION_TST)
	gzip -f $<

$(SUBMISSION_TST): $(ID_TST) $(PREDICT_TST)
	./src/write_submission.py --id-file=$< \
                              --predict-file=$(lastword $^) \
                              --submission-file=$@

clean:: clean_$(ALGO_NAME)

clean_$(ALGO_NAME):
	-rm $(METRIC_VAL1) $(SUBMISSION_TST_GZ) $(PREDICT_VAL1) $(PREDICT_TST)
	find . -name '*.pyc' -delete

.DEFAULT_GOAL := all
