# commands
SED := sed

# directories
DIR_DATA := data
DIR_BUILD := build
DIR_SRC := src
DIR_FEATURE := $(DIR_BUILD)/feature
DIR_METRIC := $(DIR_BUILD)/metric
DIR_MODEL := $(DIR_BUILD)/model


# data files
DATA_TRN := avito_train.tsv
DATA_TST := avito_test.tsv
DATA_TRN_SAMPLE := avito_train_sample.tsv
DATA_TST_SAMPLE := avito_test_sample.tsv

# directories for the cross validation and ensembling
DIR_VAL1 := $(DIR_BUILD)/val1
DIR_TST := $(DIR_BUILD)/tst

DIRS := $(DIR_DATA) $(DIR_BUILD) $(DIR_FEATURE) $(DIR_METRIC) $(DIR_MODEL) \
        $(DIR_VAL1) $(DIR_TST)

# data files for training and predict
SUBMISSION_SAMPLE := $(DIR_DATA)/sample_submission.csv

ID_TST := $(DIR_DATA)/id.tst.txt
HEADER := $(DIR_DATA)/header.txt

# initial setup
$(DIRS):
	mkdir -p $@

$(DIR_DATA)/$(DATA_TRN_SAMPLE): $(DIR_DATA)/$(DATA_TRN)
	head -n 100 $<> $@

$(DIR_DATA)/$(DATA_TST_SAMPLE): $(DIR_DATA)/$(DATA_TST)
	head -n 100 $<> $@

$(ID_TST): $(DIR_DATA)/$(DATA_TST)
	cut -f1  $< | tail -n +2 > $@

$(HEADER): $(SUBMISSION_SAMPLE)
	head -1 $< > $@

# cleanup
clean::
	find . -name '*.pyc' -delete

clobber: clean
	-rm -rf $(DIR_DATA) $(DIR_BUILD)

.PHONY: clean clobber
